#!/bin/bash

SERVER=$1
chmod 600 ./ubuntu.key
function patching {

echo "Connecting as root to $SERVER. Please ensure if sshd is enable to accept root authentications"
ssh -t -oStrictHostKeyChecking=no root@$SERVER <<-EOF
adduser -q --disabled-password --gecos "" devops
if [ ! -f /etc/sudoers ];
then
   apt-get -y install sudo > /dev/null
fi

su - devops
mkdir -p ~/.ssh
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDZeoW27ntWLtS3+FgC2TFO/Q2cekYhDjFAzaPjEmPDejC3sH127CmUf2JZ+WCe6QKEN22MNU6SCGkFnwwDjxcVaL+h6cSEo3MVMRiLyrIjqfkAdlR1LybUIuKB8OW8HcR64IKXVclZi/LRiQJmLWOLTcU//JZMuJCsbuaGt3qdf4Yq271NZ2h8MtVAcOV4CX1JPA+DsIH2RS4Sz9KUFK70edr9sUGPhejG8ezirMy7od5WLaRgqs2tMHi3pWc7o5hW6WIDr+GkDvlPo9t7Lz6P3JbMs6+9//K09AgfthtIhExVNX8a1NCEbk2XkBpFa1SW7PDTB8wzlVL2jxHaDPjL devops@debian" > ~/.ssh/authorized_keys
chmod 600 ~/.ssh/authorized_keys
exit
chattr -i /etc/sudoers
chmod u+w /etc/sudoers
chmod 600 /etc/sudoers
rm -rf /etc/sudoers.new
cp /etc/sudoers /etc/sudoers.bkp
cp /etc/sudoers /etc/sudoers.new
echo "devops ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.new
#echo "devops  ALL=(ALL) NOPASSWD: /usr/bin/apt-get*" >> /etc/sudoers.new
#echo "devops  ALL=(ALL) NOPASSWD: /bin/cp /etc/sudoers.bkp /etc/sudoers" >> /etc/sudoers.new
#echo "devops  ALL=(ALL) NOPASSWD: /bin/chattr" >> /etc/sudoers.new
##echo "devops  ALL=(ALL) NOPASSWD: ufw" >> /etc/sudoers.new
#echo "devops  ALL=(ALL) NOPASSWD: /sbin/shutdown" >> /etc/sudoers.new
cp /etc/sudoers.new /etc/sudoers
EOF
   
}


function install {

ssh -i ./ubuntu.key -oStrictHostKeyChecking=no devops@$SERVER "sudo apt-get -y install ufw > /dev/null"
ssh -i ./ubuntu.key  -oStrictHostKeyChecking=no devops@$SERVER "sudo ufw enable > /dev/null "

clear


ssh -i ./ubuntu.key -oStrictHostKeyChecking=no devops@$SERVER "sudo /sbin/shutdown -r now"

echo "restarting server"
sleep 10

isalive

disable_features

clear

cat /tmp/uptime


}


function disable_features {

ssh -i ./ubuntu.key -t -oStrictHostKeyChecking=no devops@$SERVER <<-EOF
rm /home/devops/.ssh/authorized_keys
sudo -i
sudo cp /etc/sudoers.bkp /etc/sudoers
userdel devops
chmod u-w /etc/sudoers
chattr +i /etc/sudoers
EOF


}


function isalive {

while true; 
do
if ping -c 1 $SERVER  &> /dev/null
then
  ssh -i ./ubuntu.key -oStrictHostKeyChecking=no devops@$SERVER "uptime" > /tmp/uptime
  break
else
  echo "still on boot process"
fi
done
}



if [[ -n $SERVER ]]; then
patching
echo "install..."
sleep 1
install

else
echo "Please type ./deploy.sh <server IP>"

fi
